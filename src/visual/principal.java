package visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class principal extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nombre;
    private JTextField edad;
    private JButton adicionarButton;
    private DefaultTableModel model;
    private JTable table1;
    private JButton eliminarButton;

    public principal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Edad");
        table1.setModel(model);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String nombreVar = nombre.getText();
                int edadVar = Integer.parseInt(edad.getText());

                nombre.setText("");
                edad.setText("");

                JOptionPane.showConfirmDialog(getParent(), "Usted es" + nombreVar + " y tiene " + edadVar + " años?");

                model.addRow(new Object[]{nombreVar, edadVar});
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                model.removeRow(table1.getSelectedRow());
            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        principal dialog = new principal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
